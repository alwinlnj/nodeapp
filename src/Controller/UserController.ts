import {Request, NextFunction, Response} from 'express';
import { UserModel } from '../Models/UserModel';

export class UserController{
    public signup= async(req:Request,res:Response)=>{
        const {username,password}=req.body;
        const result= await UserModel.save_user(username,password)
        res.send(result);
    }
    public get_users= async(req:Request,res:Response)=>{
        const result= await UserModel.get_users()
        res.send(result);
    }
}
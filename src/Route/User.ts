import { Router } from 'express';
import {UserController} from '../Controller/UserController';

const userRoutes:Router=Router();
const userController=new UserController();

userRoutes.post('/signup',userController.signup)
userRoutes.get('/get-users',userController.get_users)

export default userRoutes;
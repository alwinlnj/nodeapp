import { Router } from 'express';

const testRoutes:Router=Router();

testRoutes.post('/',(req,res)=>{res.send("Post testing passed")})
testRoutes.get('/',(req,res)=>{res.send("Get testing passed")})

export default testRoutes;
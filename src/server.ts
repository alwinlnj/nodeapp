import App from './App';
import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config(); // read .env files into process.env
//console.log(require('dotenv').config()) 

const port=process.env.PORT||5000;
try {
    const dev=process.env.DEV;
    let mongoURI: string = `${process.env.MONGO_PROTOCOL}://${process.env.MONGO_USER_NAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}?authSource=admin&retryWrites=true&w=majority`;
    const mongoURIDev: string = `mongodb://localhost:27017/${process.env.MONGO_DB}`;
    console.log("Dev : "+dev);
    mongoURI=(dev==="true")?mongoURIDev:mongoURI;
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.connect(mongoURI, {
        useNewUrlParser:true,
        useFindAndModify:false,
        useUnifiedTopology:true
    }); 
    console.info(`Database connected : ${mongoURI}`);
  } catch (error) {
    console.error("Error while connecting to the database : ", error);
    process.exit(1);
  }
const app=new App();
app.express.listen(port, ()=>{
    console.log("server started on port-" + port);
});
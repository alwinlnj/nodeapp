import express from 'express';
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import testRoutes from './Route/Test';
import userRoutes from './Route/User';

class App{
    public express:express.Application;
    constructor(){
        this.express=express();
        this.setMiddleWare();
        this.setRoutes();
    }
    setRoutes(){
        this.express.use("/test",testRoutes);
        this.express.use("/user",userRoutes);
    }
    setMiddleWare(){
        this.express.use(cookieParser());
        this.express.use(bodyParser.json());
    }
}
export default App;
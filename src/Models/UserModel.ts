import mongoose,{Document} from 'mongoose';

export interface IUser extends Document{
    email:string,
    password:string
}
const User=new mongoose.Schema({
    email:{
        type:String,
        unique:true
    },
    password:{
        type:String
    }
})

export class UserModel extends mongoose.model<IUser>('user',User,'user'){
    static async save_user(email:string,password:string):Promise<mongoose.Types.ObjectId | undefined>{
        const Model=mongoose.model("userModel",User,"user")
        const user=new Model({email,password}) 
        user.save()
        return user.id;
    }
    static async get_users():Promise<any>{
        const Model=mongoose.model("userModel",User,"user")
        return Model.find()
    }
}